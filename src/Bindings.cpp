#include<boost/python.hpp>
#include<boost/python/numpy.hpp>

#include "KDTree.h"

using namespace boost::python;
namespace np = boost::python::numpy;

BOOST_PYTHON_MODULE(pieKDTree){
    Py_Initialize();
    np::initialize();
    class_<KDTree>("KDTree")
            .def("BuildTree", &KDTree::BuildTreeNp)
            .def("Insert", &KDTree::InsertNp)
            .def("kNN", &KDTree::kNNNp)
            .def("GetTreeSize", &KDTree::GetTreeSize);
}